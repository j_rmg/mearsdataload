import java.io.File
import java.net.URL
import scala.io._

import org.apache.spark.sql.{SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.TimestampType

/**
  * args(0)  => File Location of csv files
  *     example: src/main/resources/
  * args(1) => HDFS file location to store Parquet Files
  *     example: /Users/jselvin/Downloads/MearsDataLoad/spark-warehouse/
  *
  * args(2) => Attributes or Events, because the columns are different for both.
  *     example: Attributes/Events
  *
  *
  * What this code does:
  *   1. Reads all files one after other in directory:args(0) file name ending with .dat
  *   2. Parse the csv, create new column called 'date' as 2018-02-20 using an event timestamp column in the csv.
  *   3. Writes the csv as parquet file with partition as 20180220
  *
  *   Todo:
  *   1. Column names are not finalised, currently used as c0,c1...c223. This must be replaced
  *   2. Every File got filename appended in the first line, this must be taken away for this program to work.
  *   3. This command must be used after data loading in Hive, "MSCK REPAIR TABLE MULTISET"
  */
object DataInsertUpdate {

  def main(args: Array[String]): Unit = {

    val attrColumnNames = List(
      "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11", "c12", "c13", "c14", "c15", "c16", "c17", "c18", "c19", "c20",
      "c21", "c22", "c23", "c24", "c25", "c26", "c27", "c28", "c29", "c30", "c31", "c32", "c33", "c34", "c35", "c36", "c37", "c38", "c39",
      "c40", "c41", "c42", "c43", "c44", "c45", "c46", "c47", "c48", "c49", "c50", "c51", "c52", "c53", "c54", "c55", "c56", "c57", "c58",
      "c59", "c60", "c61", "c62", "c63", "c64", "c65", "c66", "c67", "c68", "c69", "c70", "c71", "c72", "c73", "c74", "c75", "c76", "c77",
      "c78", "c79", "c80", "c81", "c82", "c83", "c84", "c85", "c86", "c87", "c88", "c89", "c90", "c91", "c92", "c93", "c94", "c95", "c96",
      "c97", "c98", "c99", "c100", "c101", "c102", "c103", "c104", "c105", "c106", "c107", "c108", "c109", "c110", "c111", "c112", "c113",
      "c114", "c115", "c116", "c117", "c118", "c119", "c120", "c121", "c122", "c123", "c124", "c125", "c126", "c127", "c128", "c129", "c130",
      "c131", "c132", "c133", "c134", "c135", "c136", "c137", "c138", "c139", "c140", "c141", "c142", "c143", "c144", "c145", "c146", "c147",
      "c148", "c149", "c150", "c151", "c152", "c153", "c154", "c155", "c156", "c157", "c158", "c159", "c160", "c161", "c162", "c163", "c164",
      "c165", "c166", "c167", "c168", "c169", "c170", "c171", "c172", "c173", "c174", "c175", "c176", "c177", "c178", "c179", "c180", "c181",
      "c182", "c183", "c184", "c185", "c186", "c187", "c188", "c189", "c190", "c191", "c192", "c193", "c194", "c195", "c196", "c197", "c198",
      "c199", "c200", "c201", "c202", "c203", "c204", "c205", "c206", "c207", "c208", "c209", "c210", "c211", "c212", "c213", "c214", "c215",
      "c216", "c217", "c218", "c219", "c220", "c221", "c222", "c223"
    );

    val eventColumnNames = List(
      "c0", "c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10", "c11", "c12", "c13", "c14", "c15", "c16", "c17", "c18", "c19", "c20",
      "c21", "c22", "c23", "c24", "c25", "c26", "c27", "c28", "c29", "c30", "c31", "c32", "c33", "c34", "c35", "c36", "c37", "c38", "c39",
      "c40", "c41", "c42", "c43", "c44"
    );

    val sparksession = SparkSession.builder().master("local").appName("Spark App").
      //config("hive.metastore.warehouse.dir", "/usr/hdp/current/hive-server2/metastore").
      config("hive.exec.dynamic.partition.mode", "nonstrict").
      enableHiveSupport().getOrCreate()

    val fileName = args(0)
    val filePath = new URL(fileName).getPath
    println("Name of directory:" + filePath)

    val files = new File(new URL(args(0)).getPath).listFiles

    val attributesDelimiter = "\u001A"
    val eventsDelimiter = "\u0001"

    //print(files.mkString(","))
    var columnNames = List("");
    var delimitter = "";
    if (args(2).equalsIgnoreCase("Attributes")) {
      //columnNames = attrColumnNames
      columnNames = ColumnNames.attrRealColumnNames;
      delimitter = attributesDelimiter;
    }
    else if (args(2).equalsIgnoreCase("Events")) {
      //columnNames = eventColumnNames
      columnNames = ColumnNames.eventRealColumnNames;
      delimitter = eventsDelimiter
  }

    for ( file <- files if file.getName endsWith ".dat") {

      print("Parsing file"+ file.getName + " located at "+ file.getAbsoluteFile)

       val mearsLog = sparksession.read.format("csv").option("header", "false")
        .option("inferSchema", true).option("delimiter", delimitter).option("mode", "DROPMALFORMED")
        .load("file:///" + file.getPath).toDF(columnNames: _*)

      //mearsLog.select("c1").show(10)

      val mearsLogDate = mearsLog.withColumn("mearsdate", to_date(unix_timestamp(col(/**"c1"**/"MEARS_EVENT_TIMESTAMP"), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").cast(TimestampType)) )

      val mearsdate = mearsLogDate.select("mearsdate").head.mkString.replace("-","")

      print("writing file in mearsdate partition:"+mearsdate.mkString)
      //mearsLog2.write.format("parquet").mode(SaveMode.Append).insertInto("MULTISET")

      mearsLogDate.write.mode(SaveMode.Append).parquet(args(1)+"/mearsdate="+mearsdate)

    }

  }

}
