import org.apache.spark.sql.SparkSession

/**
  * args(0)  => Reference csv file to generate datatypes and columns
  *     example:"src/test/resources/MEARS-MP_ATTRIBUTES-20180225-051500-189189-D.dat"
  * args(1) => HDFS file location where the Parquet Files are stored, this is used in Create Query.
  *
  * args(2) => table name
  *
  * args(3) => Attributes or Events, because the columns are different for both.
  *      example: Attributes/Events
  *
  * args(4) => Database name to use.
  *
  * What this code does:
  *   1. Reads the reference file from args(0).
  *   2. Use the Reference table and spark infers schema automatically for the table.
  *   3. Drops the table, table name 'Multiset'
  *   4. Construct Create Query as external Table and creates the table in Hive.
  *     example:
  *           create EXTERNAL table MULTISET(c0 decimal(29,0),c1 string,c2 string,...c223 string)
  *           PARTITIONED BY (c223 string) STORED AS PARQUET LOCATION
  *   Todo:
  *   1. Column names are not finalised, currently used as c0,c1...c223. This must be replaced
  */
object DropCreateQuery{

  def main(args: Array[String]): Unit = {

    val sparksession = SparkSession.builder().master("local").appName("Spark App").
      //config("hive.metastore.warehouse.dir", "/usr/hdp/current/hive-server2/metastore").
      config("hive.exec.dynamic.partition", "true").
      config("hive.exec.dynamic.partition.mode", "nonstrict").
      enableHiveSupport().getOrCreate()

    val attributesDelimiter = "\u001A"
    val eventsDelimiter = "\u0001"

    //print(files.mkString(","))
    var columnNames = List("");
    var delimitter = "";
    if (args(3).equalsIgnoreCase("Attributes")) {
    delimitter = attributesDelimiter;
    columnNames = ColumnNames.attrRealColumnNames;
  }
    else if (args(3).equalsIgnoreCase("Events")) {
      delimitter = eventsDelimiter;
      columnNames = ColumnNames.eventRealColumnNames;
  }

    val mearsLog = sparksession.read.format("csv").option("header", "false").option("inferSchema", true).
      option("delimiter", delimitter).load(args(0)).toDF(columnNames: _*)

    mearsLog.show(10,false)

    mearsLog.printSchema()

    val tableName = args(2)

    val databaseName = args(4)

    sparksession.sql("USE "+databaseName )

    sparksession.sql("DROP TABLE IF EXISTS "+tableName)

    val my_schema = mearsLog.schema

    val fields = my_schema.fields

    //lets say the last field is used for partition, then it must not be used inside create columns.
    //val fieldsLessPartitionfield = fields.dropRight(1)

    var fieldStr = ""

    for ( f <- fields) {
      //fieldStr += f.name.replace("_","") + " " + f.dataType.typeName + ","
      fieldStr += f.name + " " + f.dataType.typeName + ","
    }

//    if(fields.length == realColumnNames.size)
//    {
//      var incr = 0;
//      fieldStr = "";
//      for ( f <- fields) {
//        fieldStr += realColumnNames(incr) + " " + f.dataType.typeName + ","
//        incr += 1;
//      }
//    }

    //hdfs location - /user/joseph.raj/tables/
    val createQuery = "create EXTERNAL table "+tableName+"(" + fieldStr.substring(0, fieldStr.length() - 1) +") PARTITIONED BY (mearsdate string) STORED AS PARQUET LOCATION '"+args(1)+"'"

    print("createQuery:" + createQuery)

    sparksession.sql(createQuery)

    sparksession.sql("MSCK REPAIR TABLE "+tableName)
  }

}