import org.apache.spark.sql.SparkSession

/**
  * args(0) => Database name to use.
  *
  * args(1) => table name
  **/
object MsckUtil extends App{

  val sparksession = SparkSession.builder().master("local").appName("Spark App").
    config("hive.exec.dynamic.partition", "true").
    config("hive.exec.dynamic.partition.mode", "nonstrict").
    enableHiveSupport().getOrCreate()

  sparksession.sql("USE "+ args(0) )

  sparksession.sql("MSCK REPAIR TABLE "+args(1))


  //val df = sparksession.sql("select * from MULTISET")  //

  //println("count is:")
  //df.count()

  //df.collect()

  //df.show(10,false)

  //df.printSchema()

}
