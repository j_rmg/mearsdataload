import org.apache.spark.sql.{SaveMode, SparkSession}

object CreateTableWithData{

  def main(args: Array[String]): Unit = {

    val sparksession = SparkSession.builder().master("local").appName("Spark App").
      config("hive.metastore.warehouse.dir", "usr/hdp/current/hive-server2/metastore")
      .config("hive.exec.dynamic.partition", "true").
      config("hive.exec.dynamic.partition.mode", "nonstrict")
      .enableHiveSupport().getOrCreate()

    val mearsLog = sparksession.read.format("csv").option("header", "false").option("inferSchema", true).option("delimiter", "\u001A")
      .load(args(0)).toDF()

    //mearsLog.show(10, false)

    mearsLog.printSchema()

    sparksession.sql("DROP TABLE IF EXISTS MULTISET")

    mearsLog.createOrReplaceTempView("mytempTable")

    sparksession.sql("create table MULTISET as (select * from mytempTable)")

  }

  //sparksession.sql("ALTER TABLE MULTISET ADD PARTITION(_c224='i')")

  //partitioned by (_c1 string)
  //sparksession.sql("ALTER TABLE MULTISET ADD PARTITIONED BY _c1")

  //mearsLog.write.format("parquet").mode(SaveMode.Append).insertInto("MULTISET")

  //val hiveContext = new org.apache.spark.sql.hive.HiveContext(sparksession.sparkContext)

  //hiveContext.sql("create table mytable as select * from mytempTable")

  //callsLog.write.mode("append").saveAsTable("Multiset")


  //external table
 // CREATE EXTERNAL TABLE external_parquet (c1 INT, c2 STRING, c3 TIMESTAMP)
 // STORED AS PARQUET LOCATION '/user/etl/destination';


}
