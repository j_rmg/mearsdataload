# MearsDataLoad
This utitlity loads the data from fileserver to hdfs as parquet files and create hive external tables.
Tickets used for this activity are 
https://rmgdigital.atlassian.net/browse/DES-21
https://rmgdigital.atlassian.net/browse/DES-23
-----------------------------------------------------------

Steps to run:

1. Pre-processing*
-Create Directories for Attributes and Events Separately:
-Create HDFS directories to store Attributes and Events Separately
The file seems to have 1st line as signature field, remove these lines using command

sed -i -e "1d" *

2. Executing Scripts*
Data Insertion can happen before DropCreate, as the drop create is not deleting the data but only the hive table.
Also msck repair table is handled inside dropcreate and also as separate util.

For Attributes:

$SPARK_HOME/bin/spark-submit --master local[2] --class DataInsertUpdate mears-data-load_2.11-0.3.jar file:/data/home/joseph.raj/mearsdata/attributes/ /user/joseph.raj/parquet/attributes Attributes

$SPARK_HOME/bin/spark-submit --master local[2] --class DropCreateQuery mears-data-load_2.11-0.3.jar file:/data/home/joseph.raj/mearsdata/attributes/MEARS-MP_ATTRIBUTES-20180225-050000-189188-D.dat /user/joseph.raj/parquet mears_shipper_piece Attributes dev_mears_db
For Events:

$SPARK_HOME/bin/spark-submit --master local[2] --class DataInsertUpdate mears-data-load_2.11-0.3.jar file:/data/home/joseph.raj/mearsdata/events/ /user/joseph.raj/parquet/events Events

$SPARK_HOME/bin/spark-submit --master local[2] --class DropCreateQuery mears-data-load_2.11-0.3.jar file:/data/home/joseph.raj/mearsdata/events/MEARS-MP_EVENTS-20180225-051500-189189-D.dat /user/joseph.raj/parquet/events mears_events_piece Events dev_mears_db


-----------------------------------------------------------
Explanation for drop/create:

spark-submit --class DropCreateQuery <jar file> <path-to-reference-file for schema> <hdfs file path for parquet files store> <table name> <Attributes or Events> <Database Name>
example
$SPARK_HOME/bin/spark-submit --master local[2] --class DropCreateQuery mears-data-load_2.11-0.3.jar file:/data/home/joseph.raj/mearsdata/MEARS-MP_ATTRIBUTES-20180225-050000-189188-D.dat /user/joseph.raj/parquet mears_shipper_piece Attributes dev_mears_db

args(0) => Reference csv file to generate datatypes and columns
example: file:/data/home/joseph.raj/mearsdata/MEARS-MP_ATTRIBUTES-20180225-050000-189188-D.dat
*
args(1) => HDFS file location where the Parquet Files are stored, this is used in Create Query.
*
args(2) => table name
example: mears_shipper_piece
*
args(3) => Attributes / Events
example: Events
*
args(4) => Database name to use.
example: dev_mears_db
-----------------------------------------------------------

#Explanation for insert:

spark-submit --class DataInsertUpdate <jar file> <csv files folder> <hdfs file path for parquet files store> <Attributes/Events>
example
$SPARK_HOME/bin/spark-submit --master local[2] --class DataInsertUpdate mears-data-load_2.11-0.3.jar file:/data/home/joseph.raj/mearsdata/ /user/joseph.raj/parquet Attributes

args(0) => Folder Location of csv files
example: file:/data/home/joseph.raj/mearsdata/
*
args(1) => HDFS file location to store Parquet Files
example: /user/joseph.raj/parquet
*
args(2) => Attributes or Events, because the columns and delimitter are different for both.
example: Attributes
-----------------------------------------------------------
#Explanation for Msck Util:

$SPARK_HOME/bin/spark-submit --master local[2] --class MsckUtil mears-data-load_2.11-0.5.jar dev_mears_db mears_events_piece

args(0) -> database name
args(1) -> table name
